import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import { addTask } from "../../actions/tasks";

class Form extends Component {
    state = {
        name:'',
        description: '',
    };

    static  propTypes = {
        addTask: PropTypes.func.isRequired
    };

    onChange = e => this.setState({[e.target.name]: e.target.value});

    onSubmit = e => {
        e.preventDefault();
        const {name, description} = this.state;
        const task = {name, description};
        this.props.addTask(task);
        this.setState({
            name: '',
            description: ''
        });
        console.log("submit task " + name);
    };

    render() {
        const {name, description} = this.state;
        return (
            <div className="card card-body mt-4 mb-4">
                <h2>Add Task</h2>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input className="form-control"
                               type="text"
                               name="name"
                               onChange={this.onChange}
                               value={name}/>
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <input className="form-control"
                               type="text"
                               name="description"
                               onChange={this.onChange}
                               value={description}/>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(null, { addTask })(Form);